from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse  # new
from django.utils import timezone


class ikan(models.Model):
    TELAH_DIPAKAN = (
        ('s', 'Sudah'),
        ('b', 'Belum'),
    )
    nama_ikan = models.CharField('Nama Ikan', max_length=50, null=False)
    status = models.CharField('Telah Diberi Makan',max_length=2, choices=TELAH_DIPAKAN)
    jenis = models.TextField()
    jumlah = models.FloatField('Ekor (Ekor)')
    harga = models.FloatField('Harga (Rp)')
    tgl_pakan = models.DateTimeField('Tgl Pakan', default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl_pakan']

    def __str__(self):
        return self.nama_ikan
    
    def get_absolute_url(self):
        return reverse('home_page')

class pakan(models.Model):
    JENIS_PAKAN = (
        ('1', 'Pakan Bibit'),
        ('2', 'Pakan Dewasa'),
        ('3', 'Pakan Induk'),
    )
    nama_pakan = models.CharField('Nama Pakan', max_length=50, null=False)
    jenis = models.CharField(max_length=2, choices=JENIS_PAKAN)
    kandungan = models.CharField('Kandungan Pakan', max_length=150, null=False)
    berat = models.FloatField('Kilogram (Kg)')
    tgl_exp = models.DateField('Tgl Kadaluarsa', default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    class Meta:
        ordering = ['-tgl_exp']

    def __str__(self):
        return self.nama_pakan
    
    def get_absolute_url(self):
        return reverse('home_pakan')
