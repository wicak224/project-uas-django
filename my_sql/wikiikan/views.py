from django.shortcuts import render
from .models import ikan,pakan  # new
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView

var = {
    'judul': 'Pencatatan Pemberian Pakan Ikan',
    'info': '''Pencatatan Pemberian pakan dan jenis pakan''',
    'oleh': 'Wicak'
}

def index(self):
    var['wikiikan'] = ikan.objects.values('id', 'nama_ikan', 'jenis','status','jumlah', 'harga', 'tgl_pakan', 'user').\
        order_by('nama_ikan')
    return render(self, 'wikiikan/index.html', context=var)

class detailIkan(DetailView):
    model = ikan
    template_name = 'wikiikan/detail_ikan.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
    
class createIkan(CreateView):
    model = ikan
    fields = '__all__'
    template_name = 'wikiikan/ikan_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class editIkan(UpdateView):
    model = ikan
    fields = ['nama_ikan', 'jenis','status','jumlah', 'harga', 'tgl_pakan']
    template_name = 'wikiikan/ikan_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


class deleteIkan(DeleteView):
    model = ikan
    template_name = 'wikiikan/ikan_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context    

def index_pakan(self):
    var['pakan'] = pakan.objects.values('id', 'nama_pakan', 'jenis','kandungan','berat', 'tgl_exp', 'user').\
        order_by('nama_pakan')
    return render(self, 'wikiikan/index_pakan.html', context=var)