from django.urls import path
from .views import index, detailIkan,createIkan,deleteIkan,editIkan, index_pakan
urlpatterns = [
    path('', index, name='home_page'),
    path('/pakan', index_pakan, name='home_pakan'),
    path('ikan/<int:pk>', detailIkan.as_view(),
         name='detail_ikan'),
    path('ikan/add', createIkan.as_view(), name='ikan_add'),
    path('ikan/edit/<int:pk>', editIkan.as_view(), name='ikan_edit'),
    path('ikan/delete/<int:pk>',
         deleteIkan.as_view(), name='ikan_delete'),
]