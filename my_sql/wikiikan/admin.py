from django.contrib import admin
from .models import ikan
from .models import pakan

# Register your models here.


@admin.register(ikan)
class WikiikanAdmin(admin.ModelAdmin):
    list_display = ['nama_ikan', 'status', 'jenis',
                    'jumlah', 'harga', 'tgl_pakan', 'user']
    list_filter = ['nama_ikan', 'status', 'harga', 'user']
    search_fields = ['nama_ikan', 'status', 'user']

@admin.register(pakan)
class WikiikanAdmin(admin.ModelAdmin):
    list_display = ['nama_pakan', 'jenis', 'kandungan',
                    'berat', 'tgl_exp', 'user']
    list_filter = ['nama_pakan', 'jenis','user']
    search_fields = ['nama_pakan', 'jenis', 'user']